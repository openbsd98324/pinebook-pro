# PineBook Pro Notebook (pbpro, pine64)


 SoC Rockchip RK3399, pinebook pro - Dual-core ARM Cortex-A72 MPCore processor 2x2.3GHz and Quad-core ARM Cortex-A53 MPCore processor 4x1.6GHz
 


## PineBook Pro (Recommended OS, tested and working)

Armbian Cinnamon 

Linux pinebook-pro 6.1.63-current-rockchip64 #1 SMP PREEMPT Mon Nov 20 10:52:19 UTC 2023 aarch64 GNU/Linux

OS:   "https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" 


![](medias/Screenshot-1709496110-armbian.png)


 


## The Default OS, US OEM Notebook (ARM64)

US Keyboard, OS KDE

/etc/lsb-release

Manjaro-ARM
Dist release 22.06
KDE
Manjaro ARM Linux


June 2022
https://github.com/manjaro-arm/pbpro-images/releases/download/22.06


````
Linux pinebook 5.18.0-4-MANJARO-ARM #1 SMP PREEMPT Sat May 28 19:59:19 UTC 2022 aarch64 GNU/Linux
````





https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz



 KDE Desktop

![](https://gitlab.com/openbsd98324/pinebook-pro/-/raw/main/default/pinebookpro.png)


## Scanner 


LIDE 120 tested with Armbian 


![](medias/Screenshot-1709495551-lide120.png)


![](medias/20240303-204813.jpg)

OS:   "https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" >

 


## Laser Printer 

Armbian Cinnamon 

Linux pinebook-pro 6.1.63-current-rockchip64 #1 SMP PREEMPT Mon Nov 20 10:52:19 UTC 2023 aarch64 GNU/Linux

OS:   "https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" 

Brother Laser Printer, over USB Cable

![](medias/1709515904-1-Screenshot1.png)

![](medias/1709515904-2-Screenshot2.png)

![](medias/1709515904-3-Screenshot3.png)

![](medias/1709515904-4-Screenshot4.png)



##  How to Change the Keyboard Layout 

US Layout (OEM) is default 



*KDE*

Change keyboard layout 
using
KDE desktop:

![](medias/1699212132-screenshot.png)




## Gaming

supertuxkart

![](medias/Screenshot-1709498177.png)


mednafen

![](medias/1699210368-screenshot.png)


quake3 (ioquake3),... 







## Power Supply

    Input Power: 5V DC, 15W (current limit 3A) (inside photos of the factory-provided adapter: top, bottom)
    Mechanical: 3.5mm OD / 1.35mm ID, Barrel jack
    USB-C 5V, 12.5W (current limit is at 2.5A regardless of the USB PD negotiations result)
    Only use one power input at a time, barrel jack OR USB-C (note: some powerful Type-C adapters have rather limited current for 5 V operation, and e.g. 2 A won't be enough to run the system and charge the battery at the same time, so check the specs)

Pinebook Pro power and charging explains how exactly external power supply is used to run the system and charge the battery, as there are several counter-intuitive details about the process. 

It can be reloaded with the AC/DC provided power Supply (left side) or using the usb type-c cable. Very useful to be reloaded in the car with an adapter or  a rpi4 power supply.
works too with solar denver power supply (20k).

![](medias/20231105_192404.jpg)

![](medias/20231105_192414.jpg)



## Power supply using RPI4 

Working fine.
(usb)

![](medias/1699205853-screenshot-pi4-powersup.png)

````
Battery
POWER_SUPPLY_NAME=cw2015-battery
POWER_SUPPLY_TYPE=Battery
POWER_SUPPLY_CAPACITY=0
POWER_SUPPLY_STATUS=Charging
POWER_SUPPLY_PRESENT=1
POWER_SUPPLY_VOLTAGE_NOW=3823000
POWER_SUPPLY_TIME_TO_EMPTY_NOW=0
POWER_SUPPLY_TECHNOLOGY=Li-ion
POWER_SUPPLY_CHARGE_COUNTER=3
POWER_SUPPLY_CHARGE_FULL=9800000
POWER_SUPPLY_CHARGE_FULL_DESIGN=9800000
POWER_SUPPLY_CHARGE_NOW=0
POWER_SUPPLY_CURRENT_NOW=0
````

## USB Power in car

In the car, if the supply is sufficient, it can be charged with the USB (car, usb power supply).
No need of 110/220 Volts then !! Cool way to work during long trips.



## Web Meeting 

Online voip using meet jitsi 

https://meet.jit.si/opensource



## BogoMips

![](https://gitlab.com/openbsd98324/pinebook-pro/-/raw/main/medias/bogomips-pine64-vs-rpi3b.png)




## Specs


A Powerful, Metal and Open Source ARM 64-Bit Laptop for Work, School or Fun
The Pinebook Pro is meant to deliver solid day-to-day Linux or *BSD experience and to be a compelling alternative to mid-ranged Chromebooks that people convert into Linux laptops. In contrast to most mid-ranged Chromebooks however, the Pinebook Pro comes with an IPS 1080p 14″ LCD panel, a premium magnesium alloy shell, 64/128GB of eMMC storage* (more on this later – see asterisk below), a 10,000 mAh capacity battery and the modularity / hackability that only an open source project can deliver – such as the unpopulated PCIe m.2 NVMe slot (an optional feature which requires an optional adapter). The USB-C port on the Pinebook Pro, apart from being able to transmit data and charge the unit, is also capable of digital video output up-to 4K at 60hz

14″ LCD panel

Case Dimensions and Data

    Dimensions: 329 mm x 220 mm x 12 mm (W x D x H)
    Weight: 1.26 kg
    Screws on the bottom lid
        Philips-head type screws
        M2 flat-head machine screws
        4 x short screw (used along the front edge): head diameter - 3.44 mm, thread diameter - 1.97 mm, thread length - 2.1 mm, overall length - 3.05 mm
        6 x long screw (used elsewhere): head diameter - 3.44 mm, thread diameter - 1.97 mm, thread length - 4.41 mm, overall length - 5.85 mm
    Rubber feet
        18 mm diameter
        3 mm height
        Dome shaped

SoC and Memory Specification
Rockchip RK3399.png

    Based on Rockchip RK3399

CPU Architecture

    big.LITTLE architecture: Dual Cortex-A72 + Quad Cortex-A53, 64-bit CPU 


````
Full specifications
4 x ARM Cortex A53 cores @ 1.4GHz  +  2 x ARM Cortex A72 cores @ 1.8 GHz 
14″ LCD panel
ARM Mali T860 MP4 GPU
4GB LPDDR4 RAM
1080p IPS Panel
Magnesium Alloy Shell body
Bootable Micro SD Slot
64GB of eMMC (Upgradable)
M.2 NVMe SSD slot (requires optional adapter)
SPI Flash 128Mbit
HD Digital Video Out via USB-C
USB 2.0 Host
USB 3.0 Host
USB-C (Data, Power and Video out)
Lithium Polymer Battery (10000mAH)
Stereo Speakers
Built-in 802.11ac WiFi with Bluetooth 5.0
Headphone Jack
Microphone
Front-Facing Camera (1080p)
ISO & ANSI Keyboard Variants
Privacy Switches for Camera, Microphones and BT/WiFi
Large Trackpad
UART Access via Audio Jack
Barrel Power (5V 3A) Port
````



## Weight 

r206s 1245g
- Intel® Celeron® N3050 1.6 GHz
- 29.5 cm (11.6") HD 1366 x 768 pixels LED backlight 16:9

pinebook-pro 1299g, 
The Pinebook Pro weighs 1.26kg (2.78lbs) and is on the lighter end of the scale compared to other 14-inch laptops.
14″ LCD panel

citizen ti, 100g 

openpandora 326g

cult banana 120g 

pi zero w  26g

ti-30x pro 130g 





## Quick Start Minimal (to modify the emmc)

NO NEED TO OPEN THE PINEBOOK PRO TO BOOT and ACCESS THE EMMC !!
NEVER A NEED TO OPEN!
NO NEED TO OPEN THE NOTEBOOK !


SDMMC to boot minimal (running on SD/MMC).

Tested Bootloader from SD/MMC (pbpro, pinebook pro):


https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-minimal-pbpro-23.02.img.xz









note mmcblk1 is the SD/MMC. 



## Great number of possible operating systems for the pbpro 

- Recommended:

Armbian Cinnamon 

Linux pinebook-pro 6.1.63-current-rockchip64 #1 SMP PREEMPT Mon Nov 20 10:52:19 UTC 2023 aarch64 GNU/Linux

OS:   "https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" 



- Manjaro Planet 

![](medias/1699210827-screenshot.png)

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz

https://github.com/manjaro-arm/pbpro-images/releases

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz


https://github.com/manjaro-arm/pbpro-images/releases/download/22.12/Manjaro-ARM-kde-plasma-pbpro-22.12.img.xz


https://github.com/manjaro-arm/pbpro-images/releases/download/22.12/Manjaro-ARM-minimal-pbpro-22.12.img.xz



- MATE

Running mate as example

![](medias/PBPwithDE-Mate.jpg)




## Create your custom system (rootfs, manjaro aarch64)

- Debian 

debootstrap



- Devuan 

debootstrap with daedalus, it includes taking the modules and firmare from debian cinnamon

- Create a base from pacstrap:

````
   pacstrap -i  . wmctrl  xclip  wireless_tools  wpa_supplicant net-tools sshfs terminus-font  base dhcpcd gcc make subversion openssh ncurses vim wget ncftp fltk curl curlftpfs links wget nano vim arch-install-scripts firefox ctwm m4 links nano fltk subversion mpg123 links  gcc make   wmctrl nano vim wget ctwm m4 feh  xterm  vim curlftpfs debootstrap nano less subversion ncftp  firefox mpg123 feh screen xorg xorg-xinit xterm screen 
   cp -a /lib/firmware lib/firmware 
   cp -a /lib/modules lib/modules 
````




## Boot of PineBook Pro

If running on USB pendrive, 
extlinux/extlinux.conf will indicate or rule the rootfs ... 

It depends on the eMMC content.
Default (new pbpro from retailer) will boot from emmc, however, it you change the eMMC, here the following order:


Boot Order

1. SD/MMC Slot (external right side of the notebook, e.g. 22.06 file with xzcat)
   /dev/mmcblk1p1 is a vfat

2. If present, USB 3.x left side of the notebook. it will be /dev/sda2 (rootfs)

Eventually
3. USB 2.x standard (/dev/sda2 will be then /dev/sda2 
   In case no USB pendrive is given in the left USB port.

4. eMMC i.e. /dev/mmcblk2p1





## Sound


On Manjaro, it is highly recommended to use pipewire for the sound !!

On Armbian it works great.

Linux alsamixer, speaker ON and DAC up/down.

![](medias/1687454190-screenshot.png)

![](medias/1687454206-screenshot.png)

Enable in alsamixer the sound, and set with DAC.

![](medias/1699206543-screenshot.png)

DAC 


## Preinstalled ROOTFS

On /dev/sda2 ext3, usb pendrive:
https://gitlab.com/openbsd98324/manjaro-rootfs/-/raw/main/pub/content/manjaro/aarch64/BASE5-1.2-rootfs-1684676925.tar.gz

It has gcc, svn, ... wpa wireless.



## Detailed Hardware

Bluetooth (M70) und wireless fully working.

usb-c for charge
a Raspberry Pi UK Adapter with USB C 5.1V 3A output. 

````
Module                  Size  Used by
uinput                 28672  1
snd_seq_dummy          16384  0
snd_hrtimer            16384  1
snd_seq                81920  7 snd_seq_dummy
snd_seq_device         20480  1 snd_seq
rfcomm                 86016  19
nft_fib_inet           16384  1
nft_fib_ipv4           16384  1 nft_fib_inet
nft_fib_ipv6           16384  1 nft_fib_inet
nft_fib                16384  3 nft_fib_ipv6,nft_fib_ipv4,nft_fib_inet
nft_reject_inet        16384  6
nf_reject_ipv6         20480  1 nft_reject_inet
nft_reject             16384  1 nft_reject_inet
nft_ct                 24576  9
nft_chain_nat          16384  3
nf_tables             225280  204 nft_ct,nft_reject_inet,nft_fib_ipv6,nft_fib_ipv4,nft_chain_nat,nft_reject,nft_fib,nft_fib_inet
ip6table_nat           16384  0
ip6table_mangle        16384  0
ip6table_raw           16384  0
ip6table_security      16384  0
iptable_nat            16384  0
nf_nat                 49152  3 ip6table_nat,nft_chain_nat,iptable_nat
nf_conntrack          167936  2 nf_nat,nft_ct
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         16384  1 nf_conntrack
iptable_mangle         16384  0
iptable_raw            16384  0
iptable_security       16384  0
nfnetlink              20480  2 nf_tables
ip6table_filter        16384  0
ip6_tables             32768  5 ip6table_filter,ip6table_raw,ip6table_nat,ip6table_mangle,ip6table_security
iptable_filter         16384  0
bpfilter               16384  0
bnep                   32768  2
zram                   32768  2
hci_uart              143360  0
btqca                  24576  1 hci_uart
btrtl                  24576  1 hci_uart
btbcm                  28672  1 hci_uart
btintel                45056  1 hci_uart
btsdio                 20480  0
bluetooth             745472  55 btrtl,btqca,btsdio,btintel,hci_uart,btbcm,bnep,rfcomm
mcs7830                20480  0
usbnet                 53248  1 mcs7830
mii                    20480  2 mcs7830,usbnet
ecdh_generic           16384  2 bluetooth
ecc                    36864  1 ecdh_generic
snd_soc_simple_amplifier    16384  1
brcmfmac              356352  0
hantro_vpu            143360  0
brcmutil               20480  1 brcmfmac
joydev                 28672  0
gpio_keys              20480  0
rockchip_vdec          45056  0
cfg80211              421888  1 brcmfmac
rockchip_rga           28672  0
v4l2_vp9               24576  2 rockchip_vdec,hantro_vpu
panfrost               69632  7
gpu_sched              40960  1 panfrost
v4l2_h264              16384  2 rockchip_vdec,hantro_vpu
rfkill                 36864  9 bluetooth,cfg80211
uvcvideo              106496  0
drm_shmem_helper       28672  1 panfrost
videobuf2_dma_sg       20480  1 rockchip_rga
snd_soc_simple_card    24576  0
videobuf2_dma_contig    24576  2 rockchip_vdec,hantro_vpu
videobuf2_vmalloc      20480  1 uvcvideo
v4l2_mem2mem           40960  3 rockchip_vdec,hantro_vpu,rockchip_rga
snd_soc_es8316         45056  1
snd_soc_rockchip_i2s    24576  2
snd_soc_simple_card_utils    28672  1 snd_soc_simple_card
dw_wdt                 20480  0
videobuf2_memops       20480  3 videobuf2_vmalloc,videobuf2_dma_contig,videobuf2_dma_sg
watchdog               32768  1 dw_wdt
videobuf2_v4l2         32768  5 rockchip_vdec,hantro_vpu,rockchip_rga,uvcvideo,v4l2_mem2mem
snd_soc_core          245760  5 snd_soc_simple_amplifier,snd_soc_simple_card_utils,snd_soc_rockchip_i2s,snd_soc_simple_card,snd_soc_es8316
videobuf2_common       57344  10 rockchip_vdec,videobuf2_vmalloc,videobuf2_dma_contig,videobuf2_v4l2,hantro_vpu,rockchip_rga,uvcvideo,videobuf2_dma_sg,v4l2_mem2mem,videobuf2_memops
ac97_bus               16384  1 snd_soc_core
snd_pcm_dmaengine      20480  1 snd_soc_core
cw2015_battery         20480  0
videodev              233472  7 rockchip_vdec,videobuf2_v4l2,hantro_vpu,rockchip_rga,uvcvideo,videobuf2_common,v4l2_mem2mem
mc                     61440  7 rockchip_vdec,videodev,videobuf2_v4l2,hantro_vpu,uvcvideo,videobuf2_common,v4l2_mem2mem
snd_pcm               139264  4 snd_soc_simple_card_utils,snd_soc_core,snd_soc_es8316,snd_pcm_dmaengine
snd_timer              40960  3 snd_seq,snd_hrtimer,snd_pcm
rockchip_thermal       28672  0
rockchip_saradc        24576  0
gpio_charger           20480  0
8250_dw                24576  0
crypto_user            16384  0
fuse                  135168  1
pwm_bl                 20480  0
panel_edp              36864  0
drm_dp_aux_bus         20480  1 panel_edp
rockchipdrm           139264  28
drm_cma_helper         24576  1 rockchipdrm
analogix_dp            49152  1 rockchipdrm
dw_hdmi                53248  1 rockchipdrm
cec                    73728  1 dw_hdmi
rc_core                61440  1 cec
dw_mipi_dsi            20480  1 rockchipdrm
drm_dp_helper         139264  3 panel_edp,rockchipdrm,analogix_dp
drm_kms_helper        188416  8 drm_cma_helper,drm_dp_helper,dw_mipi_dsi,rockchipdrm,dw_hdmi,analogix_dp
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
fb_sys_fops            16384  1 drm_kms_helper
drm                   524288  24 drm_cma_helper,gpu_sched,drm_dp_helper,drm_kms_helper,dw_mipi_dsi,panel_edp,drm_shmem_helper,rockchipdrm,dw_hdmi,panfrost,analogix_dp
backlight              28672  3 drm_dp_helper,pwm_bl,drm
rtc_rk808              20480  1
````


## CTWM desktop running on emmc (mmcblk2p2), sdmmc, or /dev/sda2


Create a base from pacstrap:

````
   pacstrap -i  . wmctrl  xclip  wireless_tools  wpa_supplicant net-tools sshfs terminus-font  base dhcpcd gcc make subversion openssh ncurses vim wget ncftp fltk curl curlftpfs links wget nano vim arch-install-scripts firefox ctwm m4 links nano fltk subversion mpg123 links  gcc make   wmctrl nano vim wget ctwm m4 feh  xterm  vim curlftpfs debootstrap nano less subversion ncftp  firefox mpg123 feh screen xorg xorg-xinit xterm screen 
   cp -a /lib/firmware lib/firmware 
   cp -a /lib/modules lib/modules 
````


![](medias/1699184735-screenshot.jpg)

![](medias/1699207681-screenshot.png)


![](medias/20231105_192448.jpg)

![](medias/1699225412-screenshot.png)



CTWM
Rel. 23.02
Manjaro-ARM-minimal-pbpro-23.02.img.xz
Running out of SDMMC card.


Config ~/.ctwmrc 

````

WorkSpaces {
    "One"   {"#686B9F" "white" "DeepSkyBlue3" "white" "xpm:background8.xpm" }
    "Two"   {"#686B9F" "white" "firebrick" "white"    "xpm:background1.xpm" }
    "Three" {"#727786" "white" "DarkBlue"  "white"    "xpm:background3.xpm" }
    "Four"  {"#8C5b7A" "white" "MidnightBlue" "white"  }
    "Five"   {"#686B9F" "white" "DeepSkyBlue3" "white" "xpm:background8.xpm" }
    "Six"   {"#686B9F" "white" "firebrick" "white"    "xpm:background1.xpm" }
    "Seven" {"#727786" "white" "DarkBlue"  "white"    "xpm:background3.xpm" }
    "Eight"  {"#8C5b7A" "white" "MidnightBlue" "white"  }
}


Color
{
    BorderColor           "Red"
    DefaultBackground     "blue"
    DefaultForeground     "gray85"
}



# NoDefaults
"r" =	        m4	: all	:!"    cd ;  flcommand    &  "
"t" =	        m4	: all	:!"  cd   ;  env TZ=Europe/Amsterdam  kicker    &  "
# "e" =	        m4	: all	:!"    cd ; flm   &  "

"x" =	        m4	: all	:!"   xterm -fs 15 -fa 15 -bg black -fg yellow   &  "
"c" =	        m4	: all	:!"   xterm -fs 15 -fa 15 -bg black -fg yellow    &  "

"XF86AudioLowerVolume" =     	: all	:!"    amixer -c 0 sset DAC 3- &  "
"XF86AudioRaiseVolume" =     	: all	:!"    amixer -c 0 sset DAC 3+ &  "


# "f" =	        m4	: all	:!"    links -g google.com &  "

"a" =	        m4	: window	: f.fullzoom
"w" =	        m4	: window	: f.delete 
# "r" =	        m4	: window	: f.restart 



"1" = m4 : all : f.gotoworkspace "One" 
"2" = m4 : all : f.gotoworkspace "Two"
"3" = m4 : all : f.gotoworkspace "Three" 
"4" = m4 : all : f.gotoworkspace "Four"
"5" = m4 : all : f.gotoworkspace "Five" 
"6" = m4 : all : f.gotoworkspace "Six"
"7" = m4 : all : f.gotoworkspace "Seven" 
"8" = m4 : all : f.gotoworkspace "Eight"



Button1 =	: title		        : f.move
Button1 = m	: window		: f.move
Button3 = m	: window		: f.resize



RandomPlacement "on"
# RandomPlacement "on"
# RaiseOnClick
# ClickToFocus
AutoRelativeResize			# intelligent resizing => easier



Color
{
    BorderColor           "Red"
    DefaultBackground     "blue"
    DefaultForeground     "gray85"
    BorderTileBackground  "DeepSkyBlue1" {
	"xterm"		"DeepSkyBlue3"
    }
    BorderTileForeground  "Black" {
	"xterm" "DeepSkyBlue3"
    }
    TitleBackground       "DeepSkyBlue1" {
	"xterm" "DeepSkyBlue3"
    }
    TitleForeground       "Black" {
	"xterm" "White"
    }
    MenuBackground        "#686B9F"
    MenuForeground        "white"
    MenuTitleBackground   "gray70"
    MenuTitleForeground   "White"
    IconBackground        "LightSlateBlue"
    IconForeground        "White"
    IconBorderColor       "gray85"
    IconManagerBackground "DeepSkyBlue1" {"Axe" "DeepSkyBlue3" "xload" "DeepSkyBlue2"}
    IconManagerForeground "Black"
    MapWindowBackground   "DeepSkyBlue1" {
	"root*" "Red"
	"xterm" "DeepSkyBlue3"
	"Axe"	"Yellow"
    }
    MapWindowForeground   "Black" {
	"xterm" "White"
    }
}




NoOpaqueMove
NoOpaqueResize



### NEW AREA 
Button3 =	: title		: f.menu "Menuwin"


# HideWorkSpaceManager			# start with workspace manager up
WorkSpaceManagerGeometry        "360x60+60-0" 4
menu "Menuwin" {
    "Window Menu"			f.title
    "Pin menu"		f.pin
    ""			f.separator
    "Info..."			f.identify
    "Resize"			f.resize
    "Move"			f.move
    "Focus"			f.focus
    "Occupy All"		f.occupyall
    "Close"			f.delete
    "Kill"			f.destroy
    ""				f.separator
    "Show WorkSpace Manager"	f.showworkspacemgr
    "Hide WorkSpace Manager"	f.hideworkspacemgr
    ""				f.separator
    "Show Icon Manager"	f.showiconmgr
    "Hide Icon Manager"	f.hideiconmgr
    ""				f.separator
}





````


## Word processing: Texlive, Libreoffice,...


Manjaro with texlive-bin

see file in pub/content 
packages-pacstrap++-latex-1699205309.txt

![](medias/texlive-img.png)


KDE Manjaro for pbpro includes:

Libreoffice ... 



## Multiboot

ToW Boot for pbpro

See youtube.

https://www.youtube.com/watch?v=wCVcjwpDCYo


## Pinebook Pro with Desktop XFCE4 and Devuan Daedalus (debootstrap), Experimental 

![](https://gitlab.com/openbsd98324/pinebook-pro/-/raw/main/medias/Screenshot_2024-02-06_13-36-37_xfce4_devuan.png)

working, fast and reliable.

It is a non-systemd.

## NetBSD

https://bentsukun.ch/posts/pinebook-pro-netbsd/


## Wiki


https://wiki.pine64.org/wiki/Pinebook_Pro


https://www.pine64.org/pinebook-pro/


